'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#lifecycle-hooks)
 * to customize this model
 */

module.exports = {

  lifecycles: {
    async beforeUpdate(params, data) {
      console.log('--------------------');
      console.log(params);
      console.log(data);
      console.log('--------------------');
    },

    async afterUpdate(result, params, data) {
      console.log('*********************');
      console.log(params);
      console.log(data);
      console.log(result);
      console.log('*********************');
    }
  }
};
