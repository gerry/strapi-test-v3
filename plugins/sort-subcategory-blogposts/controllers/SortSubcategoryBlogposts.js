'use strict';

/**
 * SortSubcategoryBlogposts.js controller
 *
 * @description: A set of functions called "actions" of the `sort-subcategory-blogposts` plugin.
 */
const { sanitizeEntity } = require('strapi-utils');

module.exports = {

  /**
   * Default action.
   *
   * @return {Object}
   */

  index: async (ctx) => {
    // Add your own logic here.

    // Send 200 `ok`
    ctx.send({
      message: 'ok'
    });
  },

  getCategoryItems: async (ctx) => {
    return {"message":"hello world"};
  }
};
