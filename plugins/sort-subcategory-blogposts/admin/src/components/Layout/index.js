import React, { memo } from "react";
import PropTypes from "prop-types";
import { PluginHeader } from "strapi-helper-plugin";

function Layout({ navLinks, children }) {

  return (
    <div className="container-fluid" style={{ padding: "18px 30px" }}>
      <PluginHeader
        title="Sort Subcategory Blogposts"
        description="Sort Blobposts withing a Subcategory"
      />
      {children}
    </div>
  );
}

Layout.defaultProps = {
  navLinks: [],
  children: null,
};

Layout.propTypes = {
  navLinks: PropTypes.arrayOf(PropTypes.object),
  children: PropTypes.any,
};

export default memo(Layout);
