import React from 'react';
import PropTypes from 'prop-types';
import { Select } from '@buffetjs/core';

function DropDownCategories({ selectedValue, subCategoryData, onChangeDropDown }) {

  return (
    <div className="row">
      <div className="col">
        <Select
          name="select"
          onChange={onChangeDropDown}
          options={subCategoryData}
          value={selectedValue}
        />
      </div>
    </div>
  )
}

export default DropDownCategories;