import React, { useEffect, useState } from 'react';
import { request } from "strapi-helper-plugin";
import DropDownCategories from '../DropDownCategories';
import DataTable from '../DataTable';
import { Button, Flex } from '@buffetjs/core';
import { LoadingIndicator } from '@buffetjs/styles';

function MainCategorySort() {
  const [selectedValue, setSelectedValue] = useState({id: -1, name: ''});
  const [selectedValueId, setSelectedValueId] = useState(-1);
  const [isLoading, setIsLoading] = useState(false);
  const [subCategoryData, setSubCategoryData] = useState([]);
  const [blogpostsData, setBlogpostsData] = useState([]);
  const [reorderedBlogpostsData, setReorderedBlogpostsData] = useState([]);
  const [sortRecordId, setSortRecordId] = useState(-1);

  const getSubCategories = async () => {
    try {
      setIsLoading(true);
      const { results } = await request(`/content-manager/collection-types/application::subcategory.subcategory/`, {
        method: "GET"
      });

      // setSubCategoryData(results);
      if (results && results.length > 0) {
        results.sort((a, b) => {
          const nameA = a.name.toUpperCase();
          const nameB = b.name.toUpperCase();

          if (nameA < nameB) {
            return -1;
          }
          if (nameA > nameB) {
            return 1;
          }
          return 0;
        });
        setSubCategoryData(results);
        setSelectedValue(results[0]);
        getBlogposts(results[0].id);
      }
      else {
        setSubCategoryData(results);
      }
    } catch (error) {
      strapi.notification.toggle({
        type: "warning",
        message: `Error loading subcategories`,
      });
    }

    setIsLoading(false);
  };

  const onChangeDropDown = ({ target: { value } }) => {
    const item = subCategoryData.find(val => val.name === value);
    setSelectedValue(item);
    getBlogposts(item.id)
  };

  const getBlogposts = async (categoryId) => {
    try {
      setIsLoading(true);
      const { blogposts } = await request(`/content-manager/collection-types/application::subcategory.subcategory/${categoryId}`, {
        method: "GET"
      });

      setBlogpostsData(blogposts);
      stepTwo(categoryId, blogposts);

    } catch (error) {
      strapi.notification.toggle({
        type: "warning",
        message: `Error loading data`,
      });
    }

    setIsLoading(false);
  };

  const stepTwo = async (subcatId, blogposts) => {
    try {
      setIsLoading(true);
      const { results } = await request(`/content-manager/collection-types/application::subcategory-blogposts.subcategory-blogposts/?subcategory_id=${subcatId}`, {
        method: "GET"
      });

      if (results && results[0]) {
        // To Do
        // Merge with original blogposts
        setSortRecordId(results[0].id);
        const merged_blogposts = [];
        results[0].items.blogposts.forEach(element => {
          const findIndex = blogposts.findIndex(blogpost => blogpost.id === element.id);
          if (findIndex > -1){
            merged_blogposts.push(blogposts[findIndex]);
            blogposts.splice(findIndex, 1)
          }
        });

        // check if we have unaccount elements
        if (blogposts.length > 0) {
          merged_blogposts.push(...blogposts);
        }
        setReorderedBlogpostsData(merged_blogposts);
      }
      else {
        // To Do
        // Use original sort order
        setReorderedBlogpostsData(blogposts);
        setSortRecordId(-1);
      }

    } catch (error) {
      strapi.notification.toggle({
        type: "warning",
        message: `Error checking for sort order`,
      });
      setSortRecordId(-1);
    }
    setIsLoading(false);
  }

  const saveSortingOrder = async () => {
    setIsLoading(true);

    const savedOrder = reorderedBlogpostsData.map((item) => {
      return {id: item.id};
    });

    //To Do
    // if sortRecordExists then send a PUT request
    // else send a POST request

    if (sortRecordId !== -1) {
      try {
        const { data } = await request(`/content-manager/collection-types/application::subcategory-blogposts.subcategory-blogposts/${sortRecordId}`, {
          method: 'PUT',
          body: { subcategory_id: selectedValue.id, items: { blogposts: savedOrder } },
        });

        strapi.notification.toggle({
          type: "success",
          message: `Sort order updated`,
        });
      }
      catch (error) {
        strapi.notification.toggle({
          type: "warning",
          message: `Error updating sort order`,
        });
      }
    }
    else {
      try {
        const { data } = await request(`/content-manager/collection-types/application::subcategory-blogposts.subcategory-blogposts/`, {
          method: 'POST',
          body: { subcategory_id: selectedValue.id, items: { blogposts: savedOrder } },
        });

        strapi.notification.toggle({
          type: "success",
          message: `Sort order updated`,
        });
      }
      catch (error) {
        strapi.notification.toggle({
          type: "warning",
          message: `Error saving sort order`,
        });
      }
    }
    setIsLoading(false);
  }

  useEffect(() => {
    getSubCategories();
  }, [])

  return (
    <div className='row'>
      <div className='col-12'>
        <p className='my-2'>
          {isLoading ? (
            <LoadingIndicator
              animationTime="0.6s"
              borderWidth="4px"
              borderColor="#f3f3f3"
              borderTopColor="#555555"
              size="26px"
            />
          ) : (
            <div style={{
              height: "26px",
              width: "26px"
            }}></div>
          )}
        </p>
        <div className="jumbotron">
          <h4 className="mb-4">Pick a Category</h4>
          <DropDownCategories selectedValue={selectedValue.name} subCategoryData={subCategoryData.map(item => item.name)} onChangeDropDown={onChangeDropDown} />
          <hr className="my-4" />
          <h4 className="mb-4">
            Drag &amp; Drop to Sort Blogposts
          </h4>

          <DataTable categoryId={selectedValue.id} blogpostsData={reorderedBlogpostsData} setReorderedBlogpostsData={setReorderedBlogpostsData} />

          <Flex justifyContent='flex-end' alignItems="center">
            <Button color="success" className="mt-2" onClick={saveSortingOrder}>Save</Button>
          </Flex>
          
        </div>
      </div>
    </div>
  )
}

export default MainCategorySort;