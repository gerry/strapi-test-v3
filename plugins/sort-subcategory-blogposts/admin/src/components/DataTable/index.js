import React, { useState, useEffect } from 'react';
import { GrabLarge } from '@buffetjs/icons';
import { Reorder } from 'framer-motion/dist/framer-motion';
import './styles.css';

function DataTable({ blogpostsData, setReorderedBlogpostsData }) {
  return (
    <div className="sort-category-data-Table">
      <Reorder.Group axis='y' values={blogpostsData} onReorder={setReorderedBlogpostsData}>
        {blogpostsData.map((item) => (
          <Reorder.Item key={item.id} value={item}>
            {item.Title}
            <span>
              <GrabLarge />
            </span>
          </Reorder.Item>
        ))}
      </Reorder.Group>
    </div>
  )
}

export default DataTable;