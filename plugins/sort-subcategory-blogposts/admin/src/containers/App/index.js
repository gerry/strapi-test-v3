/**
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React from "react";
// import { Redirect, Route, Switch } from "react-router-dom";
import Layout from "../../components/Layout";
import MainCategorySort from "../../components/MainCategorySort";
import pluginId from "../../pluginId";

// import useContentTypes from "../../hooks/useContentTypes";

const pathTo = (uri = "") => `/plugins/${pluginId}/${uri}`;
const navLinks = [
  {
    name: "Sort",
    to: `/plugins/${pluginId}/`,
  },
];

function App() {
  // const userContentTypes = useContentTypes();
  return (
    <Layout navLinks={navLinks}>
      <MainCategorySort />
    </Layout>
  );
}

export default App;